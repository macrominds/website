[macrominds-website-docs]: https://gitlab.com/macrominds/website-docs

# macrominds website

A modern and flexible, yet minimal, resource oriented flat file website system 
that comes with the following (replaceable) technologies out of the box:

* [Twig Templates](https://twig.symfony.com/)
* [YAML content meta data](https://en.wikipedia.org/wiki/YAML) as Frontmatter
* [Markdown content](https://en.wikipedia.org/wiki/Markdown)

It puts sensitive code away from the docroot and helps you with automatic deployment.
Its backend is extensively tested. 
Clean code is an important aspect for us.

See the [Documentation][macrominds-website-docs] for setup, usage and customization. 



## TODO

* Adapt composer.json content (and maybe more templates) to the new project's name
* Complement documentation with details
