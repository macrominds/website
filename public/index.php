<?php

use Macrominds\App;
use Macrominds\DefaultConfig;
use Macrominds\Frontmatter\FrontmatterServiceProvider;
use Macrominds\MarkdownServiceProvider;
use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\TwigServiceProvider;
use Macrominds\YamlParserServiceProvider;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../vendor/autoload.php'; // @codeCoverageIgnore

$app = new App(realpath(__DIR__ . '/..'));

$configuration = (new DefaultConfig())->merge([
    'variables.var.locale' => 'en',
    'template-engine' => [
        'debug-mode' => true,
        'exported'   => [
            'functions' => [
                'css' => function (string $name): string {
                    return "/css/{$name}";
                },
            ],
        ],
    ],
]);

$app->configure($configuration);

$app->registerServiceProviders([
    new FrontmatterServiceProvider($configuration),
    new YamlParserServiceProvider($configuration),
    new MarkdownServiceProvider($configuration),
    new TwigServiceProvider($configuration),
]);

// TODO delete the following example functions and example function calls once you took the hint

// See below on how to optionally customize your bindings for singletons and non-singletons.
// You can swap out implementations if you need to.

// uncomment to see Mocked Content on all pages
// exampleReplaceSingleton($app);

function exampleReplaceSingleton(App $app)
{
    $app->getContainer()->singleton(
        TemplateEngine::class,
        function() {
            return new class() implements TemplateEngine {
                public function render(Context $context, string $contentType): string
                {
                    return '<html><body><h1>Mocked Content</h1></body></html>';
                }
            };
        }
    );
}

// Uncomment to always request /404.html:
// exampleReplaceRequestWith404Request($app);

function exampleReplaceRequestWith404Request(App $app)
{
    $app->getContainer()->factoryObject(
        Request::class,
        function() {
            return Request::create('/404.html');
        }
    );
}

$app->run();
