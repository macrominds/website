---
title: Permanent Redirect 302
redirect: 
    location: /
    code: 302
---
# Permanent redirect