---
# Process the markdown as a Twig template first
is_template: true
# Use (/templates/)demo(.html.twig)
template: demo
# Meta title
title: Demo
# Example for a list (see markdown section)
items:
    - This is the first item of a Yaml list
    - This is the second item of a Yaml list
---
# Demo

Current time: {{ 'now'|format_datetime(pattern='ccc dd. MMMM y HH:mm \'Uhr\'', locale='de') }}
 
## Simple markdown and extra markdown
### Subline {.subline}

* list item
* list item 2
  * nested list item
  
This is a paragraph **with bold** and _underlined_ content and a [link](https://www.macrominds.de)
  
```md
# Markdown code snippet
This is a markdown code snippet.

Code snippets can indicate the language, but currently, this is not visually reflected. 
``` 

## Content can be parsed as a template too 

Parses content pages for twig template markup and makes it possible to process variables.

* {{ items.0 }}
* {{ items.1 }}

<ul>
{% for i in items %}
    <li>{{ i }}
{% endfor %}
</ul>

## template

Assign another template to switch the layout for this content page.

## HTML Markup

<div class="block" markdown="1">

### Heading

paragraph

* list
* list

</div>

<div class="block">

paragraph

</div>
