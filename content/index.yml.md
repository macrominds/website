---
title: macrominds/website-lib – flat file websites
description: based on markdown
---
# macrominds/website-lib
## fast and easy flat file websites based on markdown {.subline}

### See
* [our demo markup page](/subpath/demo.html).
* [a typical error page](/error.html).
* [a typical redirect 302](/redirect-302-to-index.html).
* [a typical 404](/it-is-not-there.html).
