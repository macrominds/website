# Change Log

All noteable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [unreleased]

Update to latest macrominds/website-lib version 0.7.1 – providing php 8.0 support.

## [0.5.0] – 2020-04-26 „Prepared Smorgasbord“

Update to latest macrominds/website-lib version 0.6.0, which contains some breaking changes that prepare for more modularity.

Also updated documentation and removed docs that are covered by the [website](https://website.uber.space).

Also emphasized the technical nature of error details. In order to style that section specially, I also introduced the optional yaml variable `type`. If it is set, it will cause a `data-type` attribute to be added to body with the specified value. This allows for special styling without the requirement to provide an additional template.

## [0.4.0] – 2020-01-17 „Tidy Topicality“

Uses the latest macrominds/website-lib version 0.5.0, which has been cleaned up quite a bit
behind the scenes.

## [0.3.0] – 2020-01-14 „Rotating Muggle“

The updated macrominds/website-lib version 0.4.0 favors manual instantiation over complex magic.
Now we are able to swap out service implementations. See examples in [index.php](public/index.php).

- Added hints on how to setup logrotate on the server
- Updated macrominds/website-lib and fixed breaking changes
- Added examples for how to swap out service implementations

## [0.2.0] – 2019-12-12 „Automatic Ant“

- Added automatic deployment

## [0.1.0] – 2019-11-30 „Reborn Chilled“

Initial version of this standalone website project skeleton 
as an extraction from the macrominds/website-lib project.
